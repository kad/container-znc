# ZNC Bouncer

Just a simple [ZNC](https://znc.in) bouncer with optional support for SSL.
Heavily based on https://github.com/jimeh/docker-znc just wanted a smaller
image

[![pipeline status](https://gitlab.com/kad-inc/container-znc/badges/main/pipeline.svg)](https://gitlab.com/kad-inc/container-znc/-/commits/main)


## Running

An ephemeral container listening on standard port 6667:

```
podman run --publish localhost:6667:6667 thekad/znc:latest
```

An ephemeral container listening on a different port:

```
podman run --publish localhost:6767:6667 thekad/znc:latest
```

A container with persistent config/data:

```
podman run --publish 6667:6667 --volume /var/lib/znc:/data:rw thekad/znc:latest
```

ZNC by default refuses to start as root, the entrypoint passes a flag to let it
run but startup will be delayed 30 seconds. In a rootless podman environment
this is fine as the root user inside the container will map to the user running
the container. In a rootful environment you really want to run it as a non root
user, for example:

```
# podman run --user=1000 --publish 6667:6667 thekad/znc:latest
```

### SSL Support

A container with a provided SSL directory will bootstrap with SSL support:

```
podman run --publish 6697:6667 --volume /path/to/your/znc/pem/files:/ssl thekad/znc:latest
```

!!! note

    Automatic SSL configuration requires at least 2 files: fullchain.pem, privkey.pem (with an optional dhparam.pem) all of them in PEM format

You can also override that:

 ```
podman run --publish 6697:6667 --env SSL_CRT=/etc/ssl/my.crt --env SSL_KEY=/etc/ssl/my.key --env SSL_DHP=/etc/ssl/dhparam.pem --volume /path/to/your/certs:/etc/ssl thekad/znc:latest
```

### Module Dependencies

Modules inside `/data/modules` (container's path) will be automatically built
on startup with `znc-buildmod`. If these modules require extra dependencies
these can be automatically installed by passing the `DEPENDENCIES` environment
variable:

```
podman run --publish 6667:6667 --env "DEPENDENCIES=curl-dev openssl-dev" thekad/znc:latest
```


## Remarks

* Default user and password is `admin:admin` CHANGE RIGHT AWAY
* In all cases, if the configuration doesn't exist, a simple configuration will
  be created (refer to znc.conf.default)
